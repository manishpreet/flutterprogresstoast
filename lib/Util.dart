import 'dart:async';

import 'package:flutter/material.dart';

import 'WidgetPosition.dart';

class Util {
 static WidgetPosition overlay = new WidgetPosition();
 static toast({String message="",BuildContext context}) {
    WidgetPosition overlay = new WidgetPosition();


   overlay.show(context: context,
        isToast: true,
        duration: new Duration(seconds: 3),
        externalBuilder: (BuildContext context) {
      return Center(
        child: Container(
            height: 50.0,
            color: Colors.transparent,
            child: Container(
              padding: EdgeInsets.only(left: 20,right: 20),
                decoration: new BoxDecoration(
                    color: Colors.black12,
                    borderRadius: new BorderRadius.all(
                        Radius.circular(40.0))),
                child: new Center(
                  child: new Text(message),
                ))),
      );
    });
  }

  static launchProgress({BuildContext context}) async {


    overlay.show(context: context,
        isToast: false,
        externalBuilder: (BuildContext context) {
          return Container(
              height: MediaQuery.of(context).size.height,
              color: Colors.black26,
              child: new Center(
                child: CircularProgressIndicator(),
              ));
        });
    await new Future.delayed(Duration(seconds: 10));

    disposeProgress();
  }

  static disposeProgress() {

    if(overlay!=null)
      overlay.hide();

  }
}