import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_appp/ShowingWidget.dart';

class WidgetPosition {
  bool _isVisible = false;
  OverlayEntry overlayEntry;
  ///
  /// BuildContext context: the context from which we need to retrieve the Overlay
  /// WidgetBuilder externalBuilder: (compulsory) external routine that builds the Widget to be displayed
  /// Duration duration: (optional) duration after which the Widget will be removed
  /// Offset position: (optional) position where you want to show the Widget
  ///
  void show({
    @required BuildContext context,
    @required WidgetBuilder externalBuilder,
    @required bool isToast,
    Duration duration = const Duration(seconds: 2),
  }) async {

    // Prevent from showing multiple Widgets at the same time
    if (_isVisible){
      return;
    }

    _isVisible = true;

    OverlayState overlayState = Overlay.of(context);

    overlayEntry = new OverlayEntry(
      builder: (BuildContext context) => new ShowingWidget(
        widget: externalBuilder(context),
        isToast: isToast,
      ),
    );
    overlayState.insert(overlayEntry);
if(isToast) {
  await new Future.delayed(duration);

  overlayEntry.remove();

  _isVisible = false;
}
  }
  hide()
  {

   if(overlayEntry!=null)
     {
       overlayEntry.remove();

       _isVisible = false;
     }

  }
}