import 'package:flutter/material.dart';
import 'package:flutter_appp/WidgetPosition.dart';
import 'package:flutter_appp/Util.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

 @override
  Widget build(BuildContext context) {
       return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
     mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            InkWell(
              onTap: ()=>Util.launchProgress(context: context),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  'show progress its automatic hide after 10 second to set mannuly call Util.HIdeProgress Method',
                ),
              ),
            ),
            InkWell(
              onTap: ()=>Util.toast(message: "Manish here",context: context),
              child: Text(
                'show Toast',
                style: Theme.of(context).textTheme.display1,
              ),
            ),
          ],
        ),
   ));
  }


}
